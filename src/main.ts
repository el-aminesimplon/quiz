// mon tableau de question
let questions = [
    "combien y-a-t'il de pilote(baquet) en formule 1?",
    "combien y a-t-il d équipe constructrice(ecurie) en formule 1?",
    "Quel est le champion du monde de formule 1 en 2021?",
    "Quel pilote actuel est 7 fois(co-recordman) champion du monde de formule 1?",
    "Quel pilote a gagné le championnat du monde de formule 1 en 2016?",]

// Mon tableau dans un tableau de reponse possible
let answers = [
    ["5 pilotes", "10 pilotes", "15 pilotes", "20 pilotes"],
    ["20 equipes", "5 equipes", "10 equipes", "15 equipes"],
    ["L.Hamilton", "F.Alonso", "C.Leclerc", "M.Verstappen"],
    ["N.Rosberg", "L.Hamilton", "M.Verstappen", "F.Alonso"],
    ["F.Alonso", "L.Hamilton", "N.Rosberg", "S.Vettel"],
]

// Mon tableau de reponse correcte
let correct = [
    "20 pilotes",
    "10 equipes",
    "M.Verstappen",
    "L.Hamilton",
    "N.Rosberg",
]

// Mon tableau d'image d'illustration
let illustratorImage = [
    "https://static.cnews.fr/sites/default/files/formule_1_saison_2022_tout_savoir-taille1200_62334ed20d1a1_0.jpg",
    "https://cdn-europe1.lanmedia.fr/var/europe1/storage/images/europe1/sport/formule-1-dates-horaires-et-diffusions-decouvrez-le-calendrier-de-la-saison-2023-4167409/59804307-1-fre-FR/Formule-1-dates-horaires-et-diffusions-decouvrez-le-calendrier-de-la-saison-2023.jpg",
    "https://img.lemde.fr/2023/10/29/0/0/4368/2912/664/0/75/0/b3715a5_0ee5987c804b4685a818ddc268315e69-1-7d6de917f1a94afebfb27fa8554304cc.jpg",
    "https://static.cnews.fr/sites/default/files/styles/image_750_422/public/formule_1_pilotes_palmares_lewis_hamilton_6342a0236edf7_0.jpg?itok=xqpoJAA6",
    "https://imgresizer.eurosport.com/unsafe/1200x0/filters:format(jpeg)/origin-imgresizer.eurosport.com/2014/04/20/1221315-25479357-2560-1440.jpg"
]

// Mes sectors d'element HTML
let questionH2 = document.querySelector<HTMLElement>("h2");
let btnAnswer = document.querySelectorAll<HTMLElement>(".A");
let section = document.querySelector<HTMLElement>(".section1");
let section2 = document.querySelector<HTMLElement>(".section2");
let paraph = document.querySelector<HTMLElement>("p");
let commentaire = document.querySelector<HTMLElement>(".comment");
let result = document.querySelector<HTMLElement>(".section2  .score");
let image = document.querySelector<HTMLElement>("img");
let chrono = document.querySelector<HTMLElement>("#chrono");


questions[0];
let index = 0;
let score = 0;
name();


for (const [i, selectorAnswer] of btnAnswer.entries()) {

    selectorAnswer.addEventListener("click", () => {

        // Ma condition de comparaison des reponse possible et de la reponse correcte.
        if (answers[index][i] == correct[index]) {

            commentaire.innerHTML = "Bonne reponse!!!";
            commentaire.style.color = "white";
            commentaire.style.backgroundColor = "green";
            score++

        } else {
            commentaire.innerHTML = "Mauvaise reponse!!!";
            commentaire.style.backgroundColor = "red";
            commentaire.style.color = "white";
        }

        // Mon timeout pour l'affichage du resultat si on a repondu juste ou pas.
        setTimeout(() => {
            commentaire.innerHTML = "";
        }, 1100);

        index++

        // Ma condition qui fait qu'a la fin de mon quiz, ça cache ma pop-up et m'affiche que le c'est fini et le score totale que j'ai eu. (coupler a un setTimeout pour voir si on a repondu juste a la dernieres question ou pas)
        if (index == questions.length) {
            setTimeout(() => {
            index = 0
            section.style.display = "none";
            section2.style.display = "block";
            },1100)
            if (score > 2) {

                paraph.textContent = "BRAVO!!! You are the best";

            } else {

                paraph.textContent = "PAS TOP, Bouhhh!!!";

            }
            result.style.backgroundColor = "green";
            result.style.color = "white";
            result.innerHTML = "vous avez obtenu " + score + " bonnes reponses sur " + questions.length+ " (Recharge la page pour rejouer)";

            return;
        }
        name();
    })
}

// Ma fonction qui boucle mes questions pour changer de question a chaque fois qu'on repond a une question.(coupler a un seTimeout d'une seconde avant la question suivante).
function name() {
    setTimeout(() => {
        
        questionH2.innerHTML = questions[index];
        
        image.setAttribute("src", illustratorImage[index])
        
        for (let i = 0; i < answers[index].length; i++) {
            btnAnswer[i].textContent = answers[index][i];
        }
    }, 1100);
}

// Ma fonction du compte a rebourd 

let sec = 90;
function time() {
    setInterval(() => {
        chrono.style.fontSize = "1.5em";
        chrono.style.color = "red";
        chrono.style.fontWeight = "bold";
        
        chrono.innerHTML = "Vous avez : " + sec + " secondes";
        sec--;

        // Ma condition lorsque le compte a rebourd arrive a zero.
        if (sec == 0) {
            section.style.display = "none";
            section2.style.display = "block";
            paraph.textContent = "temps écoulé";
        }

    }, 1000)
}
time();
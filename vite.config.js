import { resolve } from 'path'
import { defineConfig } from 'vite'

export default defineConfig({
    build: {
        rollupOptions: {
            input: {
                main: resolve(__dirname, 'index.html'),
                chargement: resolve(__dirname, 'chargement.html'),
                quiz: resolve(__dirname, 'quiz.html'),

            },
        },
    },
})